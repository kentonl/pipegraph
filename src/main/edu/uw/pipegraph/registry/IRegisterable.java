package edu.uw.pipegraph.registry;

public interface IRegisterable {
	String getKey();
}
